from RepurposeDB import app, drugHeader, diseaseHeader, alphaDrugDict, base_path,\
     alphaDiseaseDict, searchDict, searchTerms, annotationHeader, hpo_list, do_list,\
     drugIDLookup, diseaseIDLookup, annotationList, annotationDict, d3DataDict, drugBankLookup, labelMap,\
     ADMETdict, drugBankIDLookup, EHRdict, pathways as pathwayDict, CUIdict, evidence, evidenceTypes, pvs
from flask import Flask, request, redirect, url_for, \
     abort, render_template, make_response, jsonify, session, send_file
import sys, time, unicodedata, string, re
from forms import SearchForm, SubmissionForm, RxPredForm
from werkzeug.contrib.cache import SimpleCache
from file import read_csv, write_csv, read_file, allowed_file
from search import Pagination
from parseData import expand
from prepareIndicationsForD3V3 import getjsonForD3V3 
from werkzeug import secure_filename
from wtforms import Form
from operator import itemgetter
from flaskIndexHelper import processRxPredForm
import json, os, time, tempfile, subprocess, re
from flask.ext.mail import Mail, Message
from jinja2 import environment
from email.mime.text import MIMEText
#from prepareDataForRxPred import smileToMolecule, annotateMolecule
#from pybel import readfile as pyread

DEBUG = True
PER_PAGE = 10
mail = Mail(app)

#This generates a paginated url. The optional terms value lets you add additional values to the url.
def url_for_other_page(page, terms = {}):
    args = request.view_args.copy()
    args['page'] = page
    for key, value in terms.items():
        args[key] = value
    return url_for(request.endpoint, **args)
app.jinja_env.globals['url_for_other_page'] = url_for_other_page

#To link to pubmed, you need to get a bunch of values and parse them. This does that here.
def url_for_pubmed(entry):
    outString = ['http://www.ncbi.nlm.nih.gov/pubmed/?term="', entry[1].replace(" ", "+"), '"+AND+("']
    counter = 0 #for checking if we have the first entry yet so as to not put too many ORs
    for col in entry[4:8]:
        if len(col) >0:
            for disease in expand(col):
                if counter > 0:
                    outString.append('"+OR+"')
                outString.append(disease.replace(" ", "+"))
                counter += 1
    outString.append('")')
    return "".join(outString)
app.jinja_env.globals['url_for_pubmed'] = url_for_pubmed

table = dict.fromkeys(i for i in xrange(sys.maxunicode)
                      if unicodedata.category(unichr(i)).startswith('P') or unicodedata.category(unichr(i)).startswith('Z'))
def stripPunctuation(s):
    if isinstance(s, str):
        return s.translate(None, string.punctuation+string.whitespace)
    elif isinstance(s, unicode):
        return s.translate(table)
app.jinja_env.globals['stripPunctuation'] = stripPunctuation

#The expand function in file.py is useful in an html context
app.jinja_env.globals['expand'] = expand

@app.route('/')
@app.route('/index')
def index():
    return render_template('index.html', searchTerms = searchTerms)

@app.route('/<path:path>')
@app.route('/warning')
def warning(path=None):
    return render_template('warning.html', searchTerms = searchTerms)

@app.route('/aboutUs')
def aboutUs():
    return render_template('aboutUs.html', searchTerms = searchTerms)

#This is the 'MIADRI' page. Has information, then has a lengthy form. This generates a csv file based on the form.
#If a form validates, then redirect to 'Thanks for submitting'. If form has errors, refresh page with errors.
@app.route('/contribute', methods=['GET', 'POST'])
def contribute():
    form = SubmissionForm(request.form)
    if request.method == 'POST':
        if form.validate_on_submit():
            try:
                csv = []
                csv.append(form.compoundName.data)
                csv.append(form.compoundIdentifiers.data)
                csv.append(form.approval.data)
                csv.append(form.primaryIndicationText.data)
                primaryIndications = form.primaryIndicationSelect.data
                if primaryIndications:
                    csv.append(";".join(primaryIndications))
                else:
                    csv.append("")
                csv.append(form.secondaryIndicationText.data)
                secondaryIndications = form.secondaryIndicationSelect.data
                if secondaryIndications:
                    csv.append(";".join(secondaryIndications))
                else:
                    csv.append("")
                csv.append(form.evidence.data)
                csv.append(form.validation.data)
                csv.append(form.references.data)
                csv.append(form.yourName.data)
                csv.append(form.yourEmail.data)
                #csv = map(lambda x: re.sub(",",'\,',x), csv)
                csv = zip(*[["General Name","Compound Identifiers","FDA Approval","Primary Indication","Optional Primary Indication","Secondary Indication","Optional Primary Indication","Evidence","Validation","References","Submitter Name","Submitter Email"],csv])
                csv = "\n".join(map(",".join,csv))
                #csv = "\n".join(["General Name,Compound Identifiers,FDA Approval,Primary Indication,Optional Primary Indication,Secondary Indication,Optional Primary Indications,Evidence,Validation,References,Submitter Name,Submitter Email", csv])
                response = make_response(csv)
                response.headers["Content-Disposition"] = "attachment; filename=contribution.csv"
                msg = Message("Submission to RepurposeDB at time: "+time.strftime("%c"),
                      sender="RepurposeDB@dudleylab.org",
                      recipients=["max.tomlinson@mssm.edu"])
                msg.body = 'Submitted at time: '+time.strftime("%c")+' by '+form.yourName.data
                msg.attach("submission_"+time.strftime("%c")+'.csv',"text/csv",csv)
                mail.send(msg)
                msgToSubmitter = Message("Thank you for submitting to RepurposeDB",
                      sender="RepurposeDB@dudleylab.org",
                      recipients=[form.yourEmail.data])
                msgToSubmitter.body="Dear "+form.yourName.data+",\nWe received your submission on " + time.strftime("%c") + ".\nA RepurposeDB team member will review your submission at the earliest available time. We look forward to including the new compound, indication, or entry in the next update of RepurposeDB.\nThank you for contributing to our community efforts to expedite drug discovery and drug repositioning using RepurposeDB.\nBest wishes,\nThe RepurposeDB Team\nhttp:///repurposedb.dudleylab.org"
                mail.send(msgToSubmitter)
                #write_csv(base_path+"data/submissions/"+form.compoundName.data+" "+time.strftime("%c")+".csv", filecsv)
                return redirect(url_for("thanks"))
            except Exception:
                log.e
    return render_template('contribute.html', searchTerms = searchTerms, form = form, 
                           hpo_list = hpo_list, do_list=do_list)

#This command generates asynchronous annotation terms for the javascript annotation terms selectable in MIADRI
@app.route('/annotationterms',methods=['GET'])
def annotationterms():
    return json.dumps(annotationList)
            
@app.route('/disclaimer')
def disclaimer():
    return render_template('disclaimer.html', searchTerms = searchTerms)

@app.route('/privacyPolicy')
def privacy():
    return render_template('privacyPolicy.html', searchTerms = searchTerms)

@app.route('/statistics')
def statistics():
    return render_template('statistics.html', searchTerms = searchTerms)

@app.route('/thanks')
def thanks():
    return render_template('thanks.html', searchTerms = searchTerms)

@app.route('/help')
def help():
    return render_template('help.html', searchTerms = searchTerms)

@app.route('/explore')
def mobileSearch():
    return render_template('search.html', searchTerms = searchTerms)

# @app.route('/miadri')
# def miadri():
#     return render_template('miadri.html', searchTerms = searchTerms)

@app.route('/compare', methods=['GET', 'POST'])
def RxPRED():
    form = RxPredForm(request.form)
    focusFileInput=False
    focusProtein = False
    errors = []
    if request.method == 'POST':
        if form.validate_on_submit():
            [errors, focusFileInput, focusProtein] = processRxPredForm(request, session, base_path)
    if len(errors) == 0 and not(focusProtein) and 'RxPredFilename' in session and 'chemcomparison' in session:
        return render_template('RxPRED.html', chemcomparison = session['chemcomparison'], searchTerms = searchTerms, form = form, sdfFile=True, focusFileInput = str(focusFileInput), focusProtein = str(focusProtein))
    elif len(errors) == 0 and 'ProteinAlignment' in session:
        return render_template('RxPRED.html', proteinAlignment = url_for("proteinJSON", filename = session['ProteinAlignment']), drugBankIDLookup = json.dumps(drugBankIDLookup), searchTerms = searchTerms, form = form, focusFileInput = str(focusFileInput), focusProtein = str(focusProtein))
        
    if len(errors)>0:
        if focusProtein:
            if focusFileInput:
                form.proteinFile.errors = errors
            else:
                form.protein.errors = errors
        else:
            if focusFileInput:
                form.sdfFile.errors = errors
            else:
                form.smile.errors = errors
    return render_template('RxPRED.html', searchTerms = searchTerms, form = form, focusFileInput = str(focusFileInput), focusProtein = str(focusProtein))

@app.route('/proteinJSON/<filename>')
def proteinJSON(filename):
    return send_file(base_path+'data/BLAST/results/'+filename, mimetype='text/json')

#searchTerms for ajax query
@app.route('/searchTerms/<term>')
def getSearchTerms(term):
    return json.dumps(filter(lambda x: term.lower() in x.lower(), searchTerms))


def search_cmp(x,y):
    x = re.sub('R','A',x)
    y = re.sub('R','A',y)
    return cmp(x,y)
    
# This is called when you click the submit button on the search bar.
@app.route('/explore', defaults={'query':None, 'page': 1}, methods=['POST'])
@app.route('/explore/<query>/<page>')
def search (query, page):
    #When you're submitting a search, gets the query from the select box
    if request.method == 'POST':
        query = request.form.get("query")
    
    query = query.lower() #searchDict has lowercase keys
    ids = set(map(itemgetter(0),
                  filter(lambda x: any(query in col for col in map(str.lower,x[1])), 
                         searchDict.items()))
              ).union(map(itemgetter(1),
                          filter(lambda x: query in x[0].lower(), 
                                 labelMap.items()))
                      )
    
    results = sorted(map(lambda x: searchDict[x], list(ids)), 
                     key = itemgetter(0), cmp=search_cmp)
    print results
    #Check to make sure you selected something, and that you didn't try and manually enter an incorrect search term
    #if len(query) > 0 and query in searchDict:
    if len(results):
        if len(results) > PER_PAGE:
            return render_template('searchResults.html', query = query, page = int(page), 
                                   header = drugHeader, results = results, PER_PAGE = PER_PAGE, 
                                   pagination = Pagination(int(page), PER_PAGE, len(results)), 
                                   searchTerms = searchTerms, drugIDLookup = drugIDLookup, 
                                   diseaseIDLookup = diseaseIDLookup)
        else:
            return render_template('searchResults.html', query = query, page = int(page), 
                                   header = drugHeader, results = results, PER_PAGE = PER_PAGE, 
                                   searchTerms = searchTerms, drugIDLookup = drugIDLookup, 
                                   diseaseIDLookup = diseaseIDLookup)
    #Show error page if you manually entered an incorrect search term
    else:
        return render_template('search.html', error = query)
    #If you submit an empty value, ie, just hit the submit button without selecting a value, displays every entry.

#This browses through the drug list alphabetically
@app.route('/browseDrugs/', defaults={'curLetter': 'A', 'page': 1})
@app.route('/browseDrugs/<curLetter>/<page>')
def browseDrugs (curLetter, page):
    emptyLetters = []
    for letter in ['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','Other']:
            if not letter in alphaDrugDict:
                emptyLetters.append(letter) 
    if curLetter in alphaDrugDict:
        if len(alphaDrugDict[curLetter]) > PER_PAGE*2:
            return render_template('browseDrugs.html', letter = curLetter, page = int(page), 
                                   header = drugHeader, results = alphaDrugDict[curLetter], 
                                   PER_PAGE = PER_PAGE, pagination = Pagination(int(page), PER_PAGE, len(alphaDrugDict[curLetter])), 
                                   searchTerms = searchTerms, diseaseIDLookup = diseaseIDLookup, emptyLetters = emptyLetters)
        else:
            return render_template('browseDrugs.html', letter = curLetter, page = int(page), 
                                   header = drugHeader, results = alphaDrugDict[curLetter], 
                                   PER_PAGE = PER_PAGE, searchTerms = searchTerms, diseaseIDLookup = diseaseIDLookup, emptyLetters = emptyLetters)
    else: 
        return render_template('browseDrugs.html', letter = curLetter, searchTerms = searchTerms, emptyLetters = emptyLetters)

#This browses through the disease list alphabetically
@app.route('/browseDiseases/', defaults={'curLetter': 'A', 'page': 1})
@app.route('/browseDiseases/<curLetter>/<page>')
def browseDiseases (curLetter, page):
    emptyLetters = []
    for letter in ['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','Other']:
            if not letter in alphaDiseaseDict:
                emptyLetters.append(letter) 
    if curLetter in alphaDiseaseDict:
        if len(alphaDiseaseDict[curLetter]) > PER_PAGE:
            return render_template('browseDiseases.html', letter = curLetter, page = int(page), 
                                   header = diseaseHeader, results = alphaDiseaseDict[curLetter], 
                                   PER_PAGE = PER_PAGE*2, pagination = Pagination(int(page), PER_PAGE*2, len(alphaDiseaseDict[curLetter])), 
                                   searchTerms = searchTerms, drugIDLookup = drugIDLookup, emptyLetters = emptyLetters)
        else:
            return render_template('browseDiseases.html', letter = curLetter, page = int(page), 
                                   header = diseaseHeader, results = alphaDiseaseDict[curLetter], 
                                   PER_PAGE = PER_PAGE*2, searchTerms = searchTerms, drugIDLookup = drugIDLookup, emptyLetters = emptyLetters)
    else: 
        return render_template('browseDiseases.html', letter = curLetter, searchTerms = searchTerms, emptyLetters = emptyLetters)

#This displays the given page for a drug
@app.route('/drug/<id>')
def drug(id):
    #This gets the drug entry for the drug
    entry = searchDict[id.lower()]
    #This checks for the existence of drug specific files
    showPDB = False
    showSDF = False
    showImage = False
    if os.path.isfile(base_path+'static/images/structures/'+id+'.png'):
        showImage = True
    if os.path.isfile(base_path+"static/pdb/"+id+".pdb"):
        showPDB = True
    if os.path.isfile(base_path+"static/sdf/"+id+".sdf"):
        showSDF = True
    
    #Get evidence
    if id in evidence:
        drugEvidence = evidenceTypes[evidence[id]['EHR']][evidence[id]['SGA']][evidence[id]['P-CT']]
    else:
        drugEvience = None
    
    #Checks if this drug is part of the D3 graph, and, if so, grabs all the descriptors this drug has as part of that.
    showD3 = False
    header = []
    if id in d3DataDict:
        showD3 = True
        header = sorted(d3DataDict[id].keys(), key=str.lower)
    
    diseases = []
    for col in entry[4:8]:
#         if len(col)>0:
#             col = col[0].upper()+col[1:]
        diseases.append(expand(col))

    #This grabs the drug interactions from the drugbank XML dict, and sorts them by inRepurposeDB or not.
    drugBankData = {}
    admet = None
    admetNA = False
    pathways = None
    if entry[2] in drugBankLookup:
        drugBankData = drugBankLookup[entry[2]]
        admet = ADMETdict[entry[2]]
        admetNA = True
        for key, value in admet.items():
            if not(key in ['Type', 'inRepurposeDB', 'Approved']) and not value == 'Not Available' and not value == '':
                admetNA = False
        if entry[2] in pathwayDict: pathways = '; '.join(pathwayDict[entry[2]]['pathways'].split(';'))
    if id in EHRdict: EHR = EHRdict[id]
    else: EHR = None
    if id in CUIdict: CUI = CUIdict[id]
    else: CUI = None
    interactionsRDB = []
    interactionsOther = []
    
    if id.capitalize() in pvs:
        localpvs = round(float(pvs[id.capitalize()]['Phenomic Variability Score']),3)
    else:
        localpvs = None
    
    if 'drug-interactions' in drugBankData and 'drug-interaction' in drugBankData['drug-interactions']:
        if 'drugbank-id' in drugBankData['drug-interactions']['drug-interaction']:
            if drugBankData['drug-interactions']['drug-interaction']['drugbank-id'].lower() in searchDict:
                interactionsRDB.append([searchDict[drugBankData['drug-interactions']['drug-interaction']['drugbank-id'].lower()][0],
                                        drugBankData['drug-interactions']['drug-interaction']['drugbank-id'], 
                                        drugBankData['drug-interactions']['drug-interaction']['name'], 
                                        drugBankData['drug-interactions']['drug-interaction']['description']])
            else:
                interactionsOther.append([drugBankData['drug-interactions']['drug-interaction']['drugbank-id'], 
                                          drugBankData['drug-interactions']['drug-interaction']['name'], 
                                          drugBankData['drug-interactions']['drug-interaction']['description']])
        else:
            for item in drugBankData['drug-interactions']['drug-interaction']:
                if item['drugbank-id'].lower() in searchDict:
                    interactionsRDB.append([searchDict[item['drugbank-id'].lower()][0], item['drugbank-id'], item['name'], item['description']])
                else:
                    interactionsOther.append([item['drugbank-id'], item['name'], item['description']])
        
    return render_template('drugPage.html', id = id, diseases = diseases, entry = entry, showImage = showImage, admet = admet, admetNA = admetNA,
                           diseaseIDLookup = diseaseIDLookup, searchTerms = searchTerms, interactionsRDB = interactionsRDB,
                           annotations = annotationDict[id], annotationHeader = annotationHeader, interactionsOther = interactionsOther, 
                           showPDB = showPDB, showSDF=showSDF, showD3=showD3, header=header, drugBankData = drugBankData, EHR = EHR, 
                           evidence = drugEvidence, pathways = pathways, CUI= CUI, pvs = localpvs)

#This prepares data to be sent to a cytoscape.js visualization of a given drugs indications. Called from html
@app.route('/drug-visualization/<id>')
def drugVis(id):
    entry = searchDict[id.lower()]
    #Sorts indications into primary and secondary. Also, does different things based on whether it's a drug or disease.
    diseaseSet = set()
    primaryIndications = []
    secondaryIndications = []
    name = entry[1]
    if id[0] == 'R':
        for col in expand(entry[4]):
            if col:
                if not col in diseaseSet: 
                    primaryIndications.append([col, diseaseIDLookup[col]])
                diseaseSet.add(col)
        for row in map(expand, entry[5:8]):
            for col in row:
                if col:
                    if not col in diseaseSet: 
                        secondaryIndications.append([col, diseaseIDLookup[col]])
                    diseaseSet.add(col)
    else:
        for col in expand(entry[4]):
            if col:
                if not col in diseaseSet: 
                    primaryIndications.append([col, drugIDLookup[col]])
                diseaseSet.add(col)
        for row in map(expand, entry[5:8]):
            for col in row:
                if col:
                    if not col in diseaseSet: 
                        secondaryIndications.append([col, drugIDLookup[col]])
                    diseaseSet.add(col)
    
    showGrid = False
    if (len(primaryIndications)+len(secondaryIndications))>9:
        showGrid = True
    return render_template('drug-vis.html', id = id, name=name, primaryIndications = primaryIndications, 
                           secondaryIndications = secondaryIndications, showGrid = showGrid)

#This page uses JSmol to generate a 3d image of a drug. Needs an SDF or PDB file. Called from the html
@app.route('/drug-visualization-JSmol/<id>/<filetype>')
def drugVisJSmol(id, filetype):
    return render_template('drug-vis-JSmol.html', id = id, filetype = filetype)
@app.route('/drug-visualization-JSmol')
def drugVisJSmol2():
    return render_template('drug-vis-JSmol.html', filename=session['RxPredFilename'].replace(base_path+'static/',""))


#This generates a d3 graph of descriptor vs descriptor, with the current drug highlighted. Called from html.
@app.route('/drug-visualization-d3', defaults={'id' : 'Rx00001', 'x_axis':'MW', 'y_axis':'bonds'})
@app.route('/drug-visualization-d3/<id>', defaults={'x_axis':'MW', 'y_axis':'bonds'})
@app.route('/drug-visualization-d3/<id>/<x_axis>/<y_axis>')
def drugVisD3(id, x_axis, y_axis):
    name = drugIDLookup[id]
    return render_template('drug-vis-d3.html', id = id, name=name, x_axis=x_axis, y_axis=y_axis, 
                           d3DataDict=d3DataDict, drugIDLookup = drugIDLookup)

#This generates a tree/radial graph of indications, as an alternative to cytoscape.
@app.route('/drug-visualization-d3-v3', defaults={'id' : 'Rx00001', 'type':'tree'})
@app.route('/drug-visualization-d3-v3/<id>/<type>')
def drugVisD3V3(id, type):
    if type == 'tree':
        return render_template('drug-vis-d3-v3-tree.html', id = id)
    else:
        return render_template('drug-vis-d3-v3-radial.html', id = id)

#The graph from drugVisD3V3 calls this to get data from AJAX.
@app.route('/drugIndicationJSON/<id>')
def drugIndicationJSON(id):
    return getjsonForD3V3(id, drugIDLookup, diseaseIDLookup, searchDict)

#Returns a csv of drug ontologies for download from the drug page.
@app.route('/drug-ontologies/<id>')
def drugOntologies(id):
    csv = []
    csv.append(",".join(annotationHeader))
    for term in annotationDict[id]:
        for row in annotationDict[id][term]:
            csv.append(",".join([id, term,",".join(row)]))
    csv = "\n".join(csv)
    # We need to modify the response, so the first thing we 
    # need to do is create a response out of the CSV string
    response = make_response(csv)
    # This is the key: Set the right header for the response
    # to be downloaded, instead of just printed on the browser
    response.headers["Content-Disposition"] = "attachment; filename="+id+"_Ontologies.csv"
    return response

#Returns a csv of the a list of the indications for download from the drug page
@app.route('/drug-indications/<id>')
def drugIndications(id):
    csv = []
    csv.append("ID,Drug,Disease,Edge_Inference_type")
    entry = searchDict[id.lower()]
    for indication in expand(entry[4]):
        if indication:
            csv.append(",".join([id, entry[1], indication, "Primary indication"]))
    for col in entry[5:8]:
        for indication in expand(col):
            if indication:
                csv.append(",".join([id, entry[1], indication, "Secondary indication"]))
    csv = "\n".join(csv)
    response = make_response(csv)
    response.headers["Content-Disposition"] = "attachment; filename="+id+"_Network.csv"
    return response

# This is the disease page. It's kinda barren, but it does show the indications.
@app.route('/disease/<id>')
def disease(id):
    entry = searchDict[id.lower()]
    drugs = []
    for col in entry[3:8]:
        drugs.append(expand(col))
    drugIndications = {}
    drugSet = set()
    hasAnnotations = False
    for drugrow in drugs:
        for drug in drugrow:
            if len(drug)>0:
                drugSet.add(drug)
                drugIndications[drug] = searchDict[drugIDLookup[drug.lower()].lower()][5:8]
                if drugIDLookup[drug.lower()] in annotationDict and entry[1].upper() in annotationDict[drugIDLookup[drug.lower()]]: hasAnnotations = True
    return render_template('diseasePage.html', id = id, entry = entry, drugs = drugs, searchTerms = searchTerms, hasAnnotations = hasAnnotations,
                           drugIDLookup = drugIDLookup, diseaseIDLookup = diseaseIDLookup, drugIndications = drugIndications, 
                           annotationDict = annotationDict, annotationHeader = annotationHeader, drugSet = sorted(drugSet))