from forms import RxPredForm
import tempfile, time, os, subprocess, re
from prepareDataForRxPred import annotateSDF
from flask import url_for
import xml.etree.ElementTree as ET
from RepurposeDB import drugBankIDLookup
from file import write_JSON, allowed_file


ALLOWED_EXTENSIONS_SMALL = '.sdf'
ALLOWED_EXTENSIONS_PROTEIN = '.fasta'

def processRxPredForm(request, session, base_path):
    form = RxPredForm(request.form)
    mol = None
    success = False
    noData = False
    focusFileInput=False
    focusProtein = False
    errors = []
    if request.files['proteinFile'] or (form.protein.data and len(form.protein.data)>0):
        focusProtein = True
    if request.files['sdfFile'] or request.files['proteinFile']:
        focusFileInput = True
    elif not(form.smile.data and len(form.smile.data)>0) and not(form.protein.data and len(form.protein.data)>0):
        noData = True
        errors.append('You did not submit any data')
    
    if not noData:
        if not focusProtein:
            errors = processSmallMoleculeForm(request, session, base_path, focusFileInput)
        else:
            errors = processProteinForm(request, session, base_path, focusFileInput)
    
    return [errors, focusFileInput, focusProtein]

def processSmallMoleculeForm(request, session, base_path, focusFileInput):
    errors = []
    form = RxPredForm(request.form)
    if not focusFileInput:
        SMILE = str(form.smile.data)
        try:
            smileFilepath = tempfile.NamedTemporaryFile(suffix='.smi', prefix=str(time.time()), dir=base_path+'data/RxPredQueries', delete=False).name
            sdfFilepath = tempfile.NamedTemporaryFile(suffix='.sdf', prefix=str(time.time()), dir=base_path+'static/RxPredSDFs', delete=False).name
            csvFilepath = tempfile.NamedTemporaryFile(suffix='.csv', prefix=str(time.time()), dir=base_path+'data/RxPredQueries', delete=False).name
            f = open(smileFilepath, 'w')
            f.write(SMILE)
            f.close()
            os.chmod(smileFilepath, 0666)
            os.chmod(sdfFilepath, 0666)
            os.chmod(csvFilepath, 0666)
            annotateSDF(sdfFilepath, csvFilepath, base_path, smilefile = smileFilepath)
            form.smile.data = None
            success = True
        except IOError as e:
            errors.append("Failed to convert '"+SMILE+"' to a SMILE - could you double check that it's accurate?")
        except Exception as e:
            print SMILE, type(e), e
            errors.append(e)
            
    elif focusFileInput and allowed_file(request.files['sdfFile'].filename, ALLOWED_EXTENSIONS_SMALL):
        try:
            sdfFilepath = tempfile.NamedTemporaryFile(suffix='.sdf', prefix=str(time.time()), dir=base_path+'static/RxPredSDFs', delete=False).name
            csvFilepath = tempfile.NamedTemporaryFile(suffix='.csv', prefix=str(time.time()), dir=base_path+'data/RxPredQueries', delete=False).name
            request.files['sdfFile'].save(sdfFilepath)
            os.chmod(sdfFilepath, 0666)
            os.chmod(csvFilepath, 0666)
            annotateSDF(sdfFilepath, csvFilepath, base_path)
            success = True
        except Exception as e:
            errors.append("Your sdf file didn't parse correctly - check your syntax?")
            print type(e), e
    
    #From before switch to commandline
#             if not focusFileInput and not noData:
#                 SMILE = str(form.smile.data)
#                 try:
#                     mol = smileToMolecule(SMILE, desc = False, memory = '512M', opt=True)
#                     form.smile.data = None
#                     moleculeFile = tempfile.mkstemp(suffix='', prefix=str(time.time()), dir=base_path+'data/RxPredQueries')
#                     sdfFile = moleculeFile[1]
#                     mol.write(filename=sdfFile, format='sdf', overwrite=True)
#                 except IOError as e:
#                     form.smile.errors = ["Failed to convert '"+SMILE+"' to a SMILE - could you double check that it's accurate?"]
#                 except Exception as e:
#                     print SMILE, type(e), e
#                     errors.append('Invalid SMILE - check your syntax?')
#                     
#             elif focusFileInput and allowed_file(request.files['sdfFile'].filename, ALLOWED_EXTENSIONS):
#                 try:
#                     moleculeFile = tempfile.mkstemp(suffix='', prefix=str(time.time()), dir=base_path+'data/RxPredQueries')
#                     sdfFile=moleculeFile[1]
#                     request.files['sdfFile'].save(sdfFile)
#                     mol = pyread("sdf", sdfFile).next()
#                 except Exception as e:
#                     errors.append("Your sdf file didn't parse correctly - check your syntax?")
#                     print type(e), e
            
    elif focusFileInput:
        errors.append('Something is wrong with your file...')
        success = False
    
    
    chemcomparison = []
    if success:
        session['RxPredFilename'] = sdfFilepath
        bashCommand = 'babel '+sdfFilepath+'  '+base_path+'static/sdf/AllRxDB.sdf -ofpt -xfFP2'
        process = subprocess.Popen(bashCommand.split(), stdout=subprocess.PIPE)
        output = process.communicate()[0]
        for row in re.split('\r|\n',output):
            dbid = row[1:8]
            prob = re.findall('(?<!\d)0$|(?<!\d)1$|0\.\d*$',row)
            if len(prob)>0:
                print url_for('drug', id= drugBankIDLookup[dbid])
                chemcomparison.append([drugBankIDLookup[dbid],prob[0], url_for('drug', id= drugBankIDLookup[dbid])])
        session['chemcomparison'] = chemcomparison
        if 'ProteinAlignment' in session: del session['ProteinAlignment']
    return errors

def processProteinForm(request, session, base_path, focusFileInput):
    errors = []
    form = RxPredForm(request.form)
    seq = str(form.protein.data)
    try:
        seqFilepath = tempfile.NamedTemporaryFile(suffix='.fasta', prefix=str(time.time()), dir=base_path+'data/BLAST/submissions', delete=False).name
        resultsFilepath = tempfile.NamedTemporaryFile(suffix='.out', prefix=str(time.time()), dir=base_path+'data/BLAST/results', delete=False).name
        os.chmod(seqFilepath, 0666)
        os.chmod(resultsFilepath, 0666)
        
        if not focusFileInput:
            f = open(seqFilepath, 'w')
            f.write(seq)
            f.close()
        elif focusFileInput and allowed_file(request.files['proteinFile'].filename, ALLOWED_EXTENSIONS_PROTEIN):
            request.files['proteinFile'].save(seqFilepath)
        elif focusFileInput:
            errors.append('Something is wrong with your file...')
        
        print 'isFile: ' + str(os.path.isfile(resultsFilepath))
        if len(errors) == 0:
            bashCommand = "/usr/local/ncbi/blast/bin/blastp  -db "+base_path+"data/BLAST/biotech_drugs.fasta -query "+seqFilepath+" -out "+resultsFilepath+" -outfmt 5"
    
            process = subprocess.Popen(bashCommand.split(), stdout=subprocess.PIPE)
            #output = process.communicate()[0]
            #print output
            process.wait()
            
            blastOutput = []
            tree = ET.parse(resultsFilepath)
            root = tree.getroot()
            prevID = None
            for hit in root.iter('Hit'):
                id = re.findall('DB[0-9]{5}',hit.find('Hit_def').text)[0]
                if id in drugBankIDLookup and not(id == prevID):
                    hitDict = {}
                    rxid = drugBankIDLookup[id]
                    hitDict['id'] = rxid
                    hsp = hit.find('Hit_hsps').find('Hsp')
                    hitDict['score'] = hsp.find('Hsp_bit-score').text
                    hitDict['identity'] = hsp.find('Hsp_identity').text
                    hitDict['positive'] = hsp.find('Hsp_positive').text
                    hitDict['gaps'] = hsp.find('Hsp_gaps').text
                    hitDict['len'] = hit.find('Hit_len').text
                    hitDict['query_seq'] = hsp.find('Hsp_qseq').text
                    hitDict['target_seq'] = hsp.find('Hsp_hseq').text
                    hitDict['consensus'] = hsp.find('Hsp_midline').text
                    blastOutput.append(hitDict)
                prevID = id
            if len(blastOutput)>0:
                write_JSON(resultsFilepath, blastOutput)
                session['ProteinAlignment'] = re.sub(base_path+'data/BLAST/results/','',resultsFilepath)
                if 'chemcomparison' in session: del session['chemcomparison']
            else:
                errors.append('No alignments to RxDB biotech drugs found.')
            
        form.protein.data = None
    except IOError as e:
        errors.append(e)
    except Exception as e:
        print seqFilepath, resultsFilepath, type(e), e
        errors.append(e)
            
    return errors