from flask.ext.wtf import Form, RecaptchaField    
from wtforms import StringField, TextField, RadioField, FileField, validators, SelectMultipleField
from wtforms.fields.html5 import EmailField  
from wtforms.widgets import TextArea



# search form
class SearchForm(Form):
    search_text = StringField ('search_string')

class RxPredForm(Form):
    smile = StringField('smile', [validators.Length(min=3, max=1000), validators.Optional()])
    sdfFile = FileField('sdfFile', [validators.Optional()])
    protein = StringField('protein', [validators.Length(min=3, max=33000), validators.Optional()], widget=TextArea())
    proteinFile = FileField('proteinFile', [validators.Optional()])

class NonValidatingSelectMultipleField(SelectMultipleField):
    def pre_validate(self, form):
        pass

class SubmissionForm (Form):
    compoundName = TextField('Compound Name', [validators.Length(min=3, max=25)])
    compoundIdentifiers = TextField('Compound Identifiers', 
                                    [validators.Length(min=2, max=25)])
    approval = RadioField('FDA Approval', 
                          choices=[('yes', 'This indication is FDA-Approved<br>'), 
                                   ('no', 'This indication is <b>NOT</b> FDA-Approved')])
    primaryIndicationText = TextField('Primary Indication', [validators.Length(min=3, max=25), validators.Optional()])
    primaryIndicationSelect = NonValidatingSelectMultipleField('Primary Indications', choices=[])
    secondaryIndicationText = TextField('Secondary Indication', [validators.Length(min=3, max=25), validators.Optional()])
    secondaryIndicationSelect = NonValidatingSelectMultipleField('Secondary Indications', choices=[])
    evidence = RadioField('Evidence',
                          choices=[('experimental', 'Experimental'), 
                                   ('computation_repurpose', 'Computational Repurpose')])
    validation = RadioField('Validation', 
                            choices=[('clinical_trial', 'Clinical Trial'),
                                     ('ehrbased_validation', 'EHR-based Validation')])
    references = StringField('Text', widget=TextArea())
    yourName = TextField('Your Name', [validators.Length(min=2, max=25)])
    yourEmail = EmailField('Your Email', [validators.DataRequired(), validators.Email()])
    
    
    recaptcha = RecaptchaField()
    
