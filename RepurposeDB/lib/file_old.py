# set of utilities to interact with files

# @author: rm3086 (at) columbia (dot) edu

import csv

# read data from a csv file    
def read_csv (filename):
	reader = csv.reader (open(filename, "rU"))
	data = []
	for r in reader:
		data.append(r)
	return data


# read a text file
def read_file (filename):
	fid = open(filename, 'r')
	data = []
	for line in fid:			
		if len(line) > 0:
			line
			data.append (line.strip())		
	fid.close()
	return data

def write_csv (filename, data, logout = True):
	try:
		doc = csv.writer (open(filename, 'wb'), delimiter=',', quotechar='"', quoting=csv.QUOTE_ALL)
		for d in data:
			doc.writerow (d)
		return True			
	except Exception as e:
		if logout is True:
			log.error(e)
		return False

def allowed_file(filename, ALLOWED_EXTENSIONS):
    return '.' in filename and \
           filename.rsplit('.', 1)[1] in ALLOWED_EXTENSIONS

def makeIDSet(filename, column):
    data = read_csv(filename)
    localSet = set()
    for row in data:
        localSet.add(row[column])
    return localSet

def makeIDLookup(filename, column, target):
    data = read_csv(filename)
    localDict = {}
    for row in data:
        localDict[row[column]] = row[target]
    return localDict