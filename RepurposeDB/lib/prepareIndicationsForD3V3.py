from flask import url_for, jsonify
from parseData import expand

def getjsonForD3V3(id, drugIDLookup, diseaseIDLookup, searchDict):
    if id[0] == 'R':
        strokecolor = '#e74c3c'
        fillcolor = 'orangered'
        selflookup = drugIDLookup
        lookup = diseaseIDLookup
    else: 
        strokecolor = 'steelblue'
        fillcolor = 'lightsteelblue'
        selflookup = diseaseIDLookup
        lookup = drugIDLookup
    name = selflookup[id]
    children = getIndicationsForJSON(id, drugIDLookup, diseaseIDLookup, searchDict, endWithIDs = False)
    localdict = {'name':name, 'children':children, 'strokecolor' :strokecolor, 'fillcolor': fillcolor}
    return jsonify(**pareIndicationsForJSON(localdict))

#This gets the indications for drugIndicationJSON. Could be easily modified to be recursive.
def getIndicationsForJSON(id, drugIDLookup, diseaseIDLookup, searchDict, endWithIDs = True, exclude = []):
    entries = searchDict[id.lower()]
    if id[0] == 'R':
        pageType = 'disease'
        strokecolor = 'steelblue'
        fillcolor = 'lightsteelblue'
        lookup = diseaseIDLookup
    else: 
        pageType = 'drug'
        strokecolor = '#e74c3c'
        fillcolor = 'orangered'
        lookup = drugIDLookup
        
    indications = [{'name':'Primary Indications','children':[], 'strokecolor' : strokecolor, 'fillcolor' : fillcolor},
                   {'name':'Orphan Indications','children':[], 'strokecolor' : strokecolor, 'fillcolor' : fillcolor},
                   {'name':'Rare Indications','children':[], 'strokecolor' : strokecolor, 'fillcolor' : fillcolor},
                   {'name':'Common Indications','children':[], 'strokecolor' : strokecolor, 'fillcolor' : fillcolor}]
    name = entries[0][1]
    
    
    for entry in entries:
        for val, row in enumerate(map(expand, entry[4:8])):
            for col in row:
                if col and not lookup[col] in exclude:
                    if endWithIDs:
                        indications[val]['children'].append(lookup[col])
                    else:
                        indications[val]['children'].append({'name':col, 'strokecolor' : strokecolor, 'fillcolor' : fillcolor, 'url': url_for(pageType, id = lookup[col])})
    
    return indications

#Removes empty values from dict generated in drugIndicationJSON 
def pareIndicationsForJSON(localdict):
    indicationTypes = ['Primary Indications','Orphan Indications','Rare Indications','Common Indications']
    
    if 'name' in localdict and 'children' in localdict:
        paredIndications = []
        for child in localdict['children']:
            child = pareIndicationsForJSON(child)
            if child: paredIndications.append(child)
        if len(paredIndications) == 0:
            if localdict['name'] in indicationTypes:
                return None
            else:
                name = localdict['name']
                return {'name':name,'strokecolor' : localdict['strokecolor'], 'fillcolor' : localdict['fillcolor']}
        localdict['children'] = paredIndications
        localdict['name'] = localdict['name']
    elif not 'name' in localdict:
        print "something has gone wrong"
        return None
    return localdict

#
#def makeLongNamesMultiLine(input):
#     input = input.split()
#     locallist = []
#     sumLength = 0
#     for word in input:
#         if sumLength > 1:
#             locallist.append('<br/>')
#             sumLength = 0
#         sumLength += len(word)
#         locallist.append(word)
#     print " ".join(locallist)
#     return " ".join(locallist)
#    return input