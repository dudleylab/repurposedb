from math import ceil, log10
from symbol import term
from operator import itemgetter
from xml.etree import ElementTree as ET
from file import read_csv, write_csv

#inputs a semicolon delimited string, outputs list of values
def expand(data):
    try:
        return map(str.strip, data.split(';'))
    except TypeError:
        return map(unicode.strip, data.split(';'))
    #return data.split(';')

#makes a twoway dict for first and second column of a table
#which in the cases used, is an ID and a name
def makeIDLookup(data):
    dict = {}
    for row in data:
        dict[row[0]] = row[1]
        dict[row[1]] = row[0]
    return dict

#makes a dictionary of the data - {letter: [[entry]..[entry]]}
def makeAlphaDict(data):
    dict = {}
    for row in data:                #for each row
        key = row[1][0]             #keys are the first letter of drug id
        if key.isdigit():           #turn uppercase, make numbers "Other"
            key = "Other"           
        else:                       
            key = key.upper()       
        if key in dict:             #if letter in dict, add to list, if not, make new key/list pair 
            dict[key].append(row)   
        else:                       
            dict[key] = [row]       
    return dict          

#{RxID:{Annotation
def makeAnnotationDict(annotationsList):
    localdict = {}
    for row in annotationsList:
        row = [x.strip('"') for x in row.split("\t")]
        if row[0] in localdict:
            if row[1] in localdict[row[0]]:
                localdict[row[0]][row[1]].append(row[2:5])
            else:
                localdict[row[0]][row[1]] = [row[2:5]]
        else:
             localdict[row[0]] = {row[1]:[row[2:5]]}
    return localdict

def makeSearchTerms(drugData, diseaseData, drugBankLookup):
    searchTerms = []
    lowerTerms = set()
    for row in drugData:                              #For each row
        for col in row:                           #and every column in that row
            for term in expand(col):              #and every term in that column
                if not(term.lower() in lowerTerms):      #if the term isn't already in the list
                    searchTerms.append(term)      #add the term to the list
                    lowerTerms.add(term.lower())
        if row[2] in drugBankLookup and 'products' in drugBankLookup[row[2]] and 'product' in drugBankLookup[row[2]]['products']:
            if not isinstance(drugBankLookup[row[2]]['products']['product'], list):
                drugBankLookup[row[2]]['products']['product'] = [drugBankLookup[row[2]]['products']['product']]
            for product in drugBankLookup[row[2]]['products']['product']:
                if 'name' in product:
                    term = str(product['name'])
                    if not(term.lower() in lowerTerms):      #if the term isn't already in the list
                        searchTerms.append(term)      #add the term to the list
                        lowerTerms.add(term.lower())
    
    for row in diseaseData:               #For each row
        term = row[0]                     #Only need disease IDs from diseaseData
        if not(term.lower() in lowerTerms):      #if the term isn't already in the list
            lowerTerms.add(term.lower())
            searchTerms.append(term)      #add the term to the list
    
    return sorted(searchTerms, key=str.lower)   #sort so that the list given to html is sorted
                
def makeSearchDict(drugData, diseaseData, drugBankLookup):
    localdict = {}
    for row in drugData:
        localdict[row[0].lower()] = row
    for row in diseaseData:
        localdict[row[0].lower()] = [row[0], row[1], "", "", row[2], row[3], row[4], row[5], ""]
    return localdict

def makeGenericLabelMap(drugData, drugBankLookup):
    localdict = {}
    for row in drugData:
        if row[2] in drugBankLookup and 'products' in drugBankLookup[row[2]] and 'product' in drugBankLookup[row[2]]['products']:
            if not isinstance(drugBankLookup[row[2]]['products']['product'], list):
                drugBankLookup[row[2]]['products']['product'] = [drugBankLookup[row[2]]['products']['product']]
            for product in drugBankLookup[row[2]]['products']['product']:
                if 'name' in product:
                    localdict[str(product['name']).lower()] = row[0].lower()
    return localdict
                    
# def makeSearchDict(drugData, diseaseData, keys):
#     localdict = {}
#     for key in keys:                                    #For each row
#         key = key.lower()
#         localdict[key] = []
#         for row in drugData:                            #iterate over every key
#             for col in row:                         #then go through every column
#                 for term in expand(col):            #and every term in the columns
#                     if key in term.lower():         #and if the key is contained in the term
#                         if not(row in localdict[key]):
#                             localdict[key].append(row)   #add the row to the dict at that key
#         for row in diseaseData:                            #iterate over every key
#             for col in row:                         #then go through every column
#                 for term in expand(col):            #and every term in the columns
#                     if key in term.lower():         #and if the key is contained in the term
#                         modrow = [row[0], row[1], "", "", row[2], row[3], row[4], row[5], ""]
#                         if not(modrow in localdict[key]):
#                             localdict[key].append(modrow)   #add the row to the dict at that key
#     return localdict

# def makeDiseaseIndexedData(drugData):
#     localdict = {}
#     counter = 1
#     for row in drugData:
#         for x, col in enumerate(row[4:8]):
#             for disease in expand(col):
#                 if disease != "":
#                     if disease in localdict:
#                         localdict[disease][x+2].append(row[1])
#                     else:
#                         ID = "Dx" + ("").join(["0"]*(5-len(str(counter)))) + str(counter)
#                         localdict[disease] = [ID, disease, [], [], [], []]
#                         localdict[disease][x+2].append(row[1])
#                         counter = counter + 1
#                     
#     diseaseData = []
#     for disease in localdict:
#         row = localdict[disease]
#         for x, col in enumerate(row[2:6]):
#             row[x+2] = (";").join(col)    
#         diseaseData.append(row)
#     return sorted(diseaseData,  key=lambda x: x[1].lower())

def makeDiseaseIndexedData(drugDataPath, diseaseDataPath, moddedDrugDataPath):
    drugDataWithHeader = read_csv(drugDataPath)
    header = drugDataWithHeader[0]
    drugData = drugDataWithHeader[1:]
    localdict = {}
    counter = 1
    lowerTerms = {}
    for row in drugData:
        for x, col in enumerate(row[4:8]):
            for disease in expand(col):
                if disease != "":
                    if disease.lower() in lowerTerms.keys():
                        if disease in localdict:
                            localdict[lowerTerms[disease.lower()]][x+2].append(row[1])
                    else:
                        lowerTerms[disease.lower()] = disease
                        ID = "Dx" + ("").join(["0"]*(6-len(str(counter)))) + str(counter)
                        localdict[disease] = [ID, disease, [], [], [], []]
                        localdict[disease][x+2].append(row[1])
                        counter = counter + 1
    
    #if there's a disease that appears in lowercase and uppercase, need to standardize it.
    for x, row in enumerate(drugData):
        for y, col in enumerate(row[4:8]):
            locallist = []
            #print col
            for disease in expand(col):
                if disease != "":
                    #print disease
                    if disease in localdict and not disease in locallist:
                        locallist.append(disease)
                    elif not (disease in locallist or lowerTerms[disease.lower()] in locallist):
                        locallist.append(lowerTerms[disease.lower()])
            drugData[x][y+4]=(';').join(locallist)
                    
    diseaseData = []
    for disease in localdict:
        row = localdict[disease]
        for x, col in enumerate(row[2:6]):
            row[x+2] = (";").join(col)    
        diseaseData.append(row)
    
    drugData.insert(0, header)
    write_csv(diseaseDataPath, sorted(diseaseData,  key=lambda x: x[1].lower()))
    write_csv(moddedDrugDataPath, drugData)
    
    #return sorted(diseaseData,  key=lambda x: x[1].lower())

def makeDrugBankLookup(drugData):
    localdict = {}
    for row in drugData:
        localdict[row[0]] = row[2]
        localdict[row[2]] = row[0]
    return localdict

def makeD3DataDict(data, drugBankLookup):
    dict = {}
    for sheet in data:
        header = sheet[0]
        for row in sheet[1:]:
            if row[0] in drugBankLookup:
                id = drugBankLookup[row[0]]
                if not (id in dict):
                    dict[id] = {}
                for x, col in enumerate(row[1:]):
                    if not 'str' in strType(col):
                        dict[id][header[x+1]] = col
    return dict

def strType(var):
    try:
        if int(var) == float(var):
            return 'int'
    except:
        try:
            float(var)
            return 'float'
        except:
            return 'str'
        
def loadDrugBankXML(filepath):
    tree = ET.parse(filepath)
    root = tree.getroot()
    dict = {}
    for drug in root:                        #For every drug in XML file 
        DBID = drug[0].text
        dict[DBID] = recursivelyIterateElements(drug)
        dict[DBID]['type'] = drug.attrib['type']
    return dict

def recursivelyIterateElements(element):     
    subElements = list(element)             #Make list of subelements
    if len(subElements) == 0:               #If there are no subelements
        if not element.text:                #Check if element has no text
            return ""
        elif len(element.attrib.keys()) > 0: 
            dict = element.attrib
            dict[parseTag(element.tag)] = element.text
            return dict
        else:
            return element.text
    else:                                   #If there ARE subelements
        dict = {}
        if len(element.attrib.keys()) > 0:
            dict.update(element.attrib)
        for subElement in subElements:      #and call this function on each subelement
            tag = parseTag(subElement.tag)
            if tag in dict:
                dict[tag].append(recursivelyIterateElements(subElement))
            else:
                dict[tag] = [recursivelyIterateElements(subElement)]
        for key in dict.keys():
            if len(dict[key]) == 1:
                dict[key] = dict[key][0]
        return dict

def makeSetDBID(drugData):
    localset = set()
    for drug in drugData:
        localset.add(drug[2])
    return localset

def parseTag(tag):
    return tag.replace("{http://www.drugbank.ca}", "")