$(document).ready(function(){
	$('.submitbutton').click(function() {
	    var form = $(this).closest("form");
	    form.submit();
	    return false;
	});
    $('#visibleonload').css('display','block');
    $(".annotationHeader").click(function(e){
        $('.'+$(this).attr('term')).toggle();
    });
	$('.clicktoexpand').click(function(e) {
		var term = $(this).attr('term')
		$(".showmore" + term).toggle();
		$(".showless" + term).toggle();
	});
    //$(".select2").selectize()
	$(".navSearch").autoComplete({
	    minChars: 2,
	    cache: false,
	    source: function(term, response){
	        $.getJSON('/searchTerms/'+term, function(data){
	        	if (data == ''){
	        		data = ['No Results Found']
	        	}
	        	response(data);
	        	});
	    },
	    renderItem: function (item, search){
	    	if (item == 'No Results Found'){
		        return '<div class="autocomplete-disabled">No Results Found</div>';
	    	}
	        search = search.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');
	        var re = new RegExp("(" + search.split(' ').join('|') + ")", "gi");
	        return '<div class="autocomplete-suggestion" data-val="' + item + '">'+ insertLineBreaks(item,30).replace(re, "<b>$1</b>") + '</div>';
	    }
	});
	$(".select2").select2();
    var $select = $('.annotation-lookup').selectize({
        plugins: ['remove_button'],
        valueField: 'id',
        labelField: 'text',
        persist: false,
        searchField: ['text'],
    	sortField: 'id'
    });
	if ($select.length > 0){
	    function loadAnnotations (callback) {
	        $.ajax({
	            url: '/annotationterms',
	            //url: '/RepurposeDB/annotationterms'
	            type: 'GET',
	            dataType: "json",
	            
	            error: function (e) {
	                console.error(e.responseText);
	                callback();
	            },
	            success: function (data) {
	                callback(data);
	            }
	        })
	    }
        var selectize0 = $select[0].selectize;
        var selectize1 = $select[1].selectize;
        selectize0.load(loadAnnotations);
        selectize1.load(loadAnnotations);
    }
    
    
})

function submitbutton(event) {
	if (!event) {
        event = window.event; // Older versions of IE use a global reference and not an argument.
    };
    var form = $(this).closest("form");
    form.submit();
    var el = (event.target || event.srcElement);
}

function insertLineBreaks(term, limit){
	var words = term.split(' ');
	var line = words[0];
	words = words.splice(1)
	var count = line.length;
	for (var word in words) {
		word = words[word];
		if (!(word === undefined)){
			count += word.length + 1;
			if (count > limit) {
				line += '<br />'+word
				count = word.length;
			}else{
				line += ' '+word
			}
		}
	}
	return line
}
