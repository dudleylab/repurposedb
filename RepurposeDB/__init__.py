from flask import Flask
from lib.file import read_csv, write_csv, read_file, csvToDictionary, csvToDictionaryList
from flask.ext.wtf import Form
from flask.ext.wtf.recaptcha import RecaptchaField
import os, sys, re
from copy import deepcopy
from operator import itemgetter
from _sqlite3 import Row
#base_path = '/opt/RepurposeDB-Python-Production/app/'
#base_path = os.getcwd()+'/'
base_path = os.path.abspath(os.path.dirname(__file__))+'/'
from lib.parseData import makeAlphaDict, makeSearchDict, makeSearchTerms, makeDrugBankLookup, \
     makeIDLookup, makeAnnotationDict, makeDiseaseIndexedData, makeD3DataDict, loadDrugBankXML, \
     makeGenericLabelMap
from lib.file import csvToDict

import json



settings = csvToDict(base_path+'data/settings.txt')
if settings:
    RECAPTCHA_PUBLIC_KEY = settings['RECAPTCHA_PUBLIC_KEY']
    RECAPTCHA_PRIVATE_KEY = settings['RECAPTCHA_PRIVATE_KEY']
    SECRET_KEY = settings['SECRET_KEY']
    
    MAIL_SERVER = settings['MAIL_SERVER']
    MAIL_PORT = settings['MAIL_PORT']
    MAIL_USE_SSL = True
    MAIL_USERNAME = settings['MAIL_USERNAME']
    MAIL_PASSWORD = settings['MAIL_PASSWORD']

app = Flask(__name__)
app.config.from_object(__name__)

drugDataPath = base_path + "data/drug_indexed_data.csv"
moddedDrugDataPath = base_path + "data/drug_indexed_data_modded.csv"
diseaseDataPath = base_path + "data/disease_indexed_data.csv"

#if disease data (which is just pivotted drug data) is not made, make it. Then laod it.
if not (os.path.isfile(moddedDrugDataPath) and os.path.isfile(diseaseDataPath)):
    makeDiseaseIndexedData(drugDataPath, diseaseDataPath, moddedDrugDataPath)
    print("Wrote Disease Indexed Data and Modded DrugData")
drugData = read_csv(moddedDrugDataPath)
diseaseData = read_csv(diseaseDataPath)

#All RepurposeDB drugs, annotations for all repurposeDB drugs
#drugData = read_csv(base_path + "data/drug_indexed_data.csv")
annotations = read_file(base_path + "data/annotations/repurposedb_annotations.csv")

#list of drug ontologies
do_list = [x.decode('utf8').split("\t") for x in read_file(base_path + "data/annotations/do_list.txt")]
hpo_list = [x.decode('utf8').split("\t") for x in read_file(base_path + "data/annotations/hpo_list.txt")]
annotationList = []
for row in do_list:
    annotationList.append({"id":row[0], "text":(": ".join([row[0], row[1]]))})
for row in hpo_list:
    annotationList.append({"id":row[0], "text":(": ".join([row[0], row[1]]))})

#get header
annotationHeader = [x.strip('"') for x in annotations[0].split("\t")]
drugHeader = drugData[0]
diseaseHeader = ["Disease", "DxID", "Primary Indication", "Orphan indications*","Rare indication","Common indication"]

#strip header
drugData = drugData[1:len(drugData)]
annotations = annotations[1:len(annotations)]



#diseaseData = diseaseData[1:len(diseaseData)]

#ID lookups translate RxID or DxID to drug / disease name
drugIDLookup = makeIDLookup(drugData)
diseaseIDLookup = makeIDLookup(diseaseData)
print("Made ID Lookups!")

#annotation dict is a dictionary of annotations indexed by RxID
annotationDict = makeAnnotationDict(annotations)
print ("Made Annotation Dictionary!")
#alphaDrugDict is {letter: [list of drug entries with drug starting at that latter]}
alphaDrugDict = makeAlphaDict(drugData)
print ("Made Alphabetical Drug Indexed Dictionary!")
#Same thing, but with diseases
alphaDiseaseDict = makeAlphaDict(diseaseData)
print ("Made Alphabetical Disease Indexed Dictionary!")

#drugBankLookup loads the drugBank xml file (but only for drugs in repurposedb)
#It's used to display a number of features found in the drugbank xml file
#It has the same structure as the drugbank xml file, except the initial keys are DBIDs
if not os.path.isfile(base_path + "data/drugBankLookup.json"):
    drugBankLookup = loadDrugBankXML(base_path + 'data/repurposeDB.xml')
    json.dump(drugBankLookup, open(base_path + "data/drugBankLookup.json", 'wb'))
    print("Made DrugBank Lookup!")
else:
    drugBankLookup = json.load(open(base_path + "data/drugBankLookup.json"))
    print ("Loaded DrugBank Lookup From File!")

#searchTerms is a list of every term found in a repurposedb drug or disease entry
#searchDict is a dictionary that links every term in any entry
#to the entries it is part of
searchTerms = makeSearchTerms(drugData, diseaseData, drugBankLookup)
labelMap = makeGenericLabelMap(drugData, drugBankLookup)
print ("Made List of Search Terms!")
if not os.path.isfile(base_path + "data/searchDict.json!"):
    #searchDict = makeSearchDict(drugData, diseaseData, searchTerms)
    searchDict = makeSearchDict(drugData, diseaseData, drugBankLookup)
    print ("Made Search Results Dictionary!")
    json.dump(searchDict, open(base_path + "data/searchDict.json", 'wb'))
else:
    searchDict = json.load(open(base_path + "data/searchDict.json"))
    print ("Loaded Search Results Dictionary From File!")

#drugbankIDlookup translates DBIDs to RxIDs
drugBankIDLookup = makeDrugBankLookup(drugData)
#this indexed all the feature data in the given links by their RxID for use in a d3 visualization
d3DataDict = makeD3DataDict([read_csv(base_path + "data/ChemicalFeaturesChemminer.csv"),
                            read_csv(base_path + "data/ChemicalFeaturesJoeLib.csv"),
                            read_csv(base_path + "data/ChemicalFeaturesOpenBabel.csv")],
                            drugBankIDLookup)
print ('Made Dictionary of Drug D3 Data!')

#Load ADMET data from webscraped csv
ADMETdict = csvToDictionary(base_path+ 'data/drugBankADMET.csv', 'Drug Bank ID')

#load pathways
pathways = csvToDictionary(base_path+ 'data/pathways.csv', 'DBID')
#load EHR
if not os.path.isfile(base_path + "data/RepurposeDB_EHR_SGA.json!"):
    EHR = csvToDictionaryList(base_path+"data/RepurposeDB_EHR_SGA.csv")
    EHRdict = {}
    for row in EHR:
        row['bonf_corr_pval'] = float(row['bonf_corr_pval'])
        if row['bonf_corr_pval'] <= 0.00001:
            index = row['d1']
            del row['d1']
            del row['total']
            row['OR'] = round(float(row['OR']),3)
            if not row['drug'] in EHRdict:
                EHRdict[row['drug']] = {index:[deepcopy(row)]}
            elif not index in EHRdict[row['drug']]:
                EHRdict[row['drug']][index] = [deepcopy(row)]
            else:
                EHRdict[row['drug']][index].append(deepcopy(row))
    for drug, drugEHRDict in EHRdict.items():
        locallist = []
        for d1, rows in drugEHRDict.items():
            rows = sorted(rows, key=itemgetter('bonf_corr_pval'), reverse = False)
            locallist.append([d1,rows])
        locallist = sorted(locallist, key=lambda x: x[1][0]['bonf_corr_pval'], reverse = False)
        EHRdict[drug] = locallist
    json.dump(EHRdict, open(base_path + "data/RepurposeDB_EHR_SGA.json", 'wb'))
else:
    EHRdict = json.load(open(base_path+"data/RepurposeDB_EHR_SGA.json"))

if not os.path.isfile(base_path + "data/repurposedb_cui.json"):
    CUI = csvToDictionaryList(base_path+"data/repurposedb_cui.txt", delimiter= '\t')
    CUIdict = {}
    for row in CUI:
        row['bonf_corr_pval'] = float(row['bonf_corr_pval'])
        if row['bonf_corr_pval'] <= 0.00001:
            index = row['d1']
            del row['d1']
            row['shared_gene_ids'] = ', '.join(sorted(re.split(',',row['shared_gene_ids'])))
            if not row['drug'] in CUIdict:
                CUIdict[row['drug']] = {index:[deepcopy(row)]}
            elif not index in CUIdict[row['drug']]:
                CUIdict[row['drug']][index] = [deepcopy(row)]
            else:
                CUIdict[row['drug']][index].append(deepcopy(row))
    for drug, drugCUIDict in CUIdict.items():
        locallist = []
        for d1, rows in drugCUIDict.items():
            rows = sorted(rows, key=itemgetter('bonf_corr_pval'), reverse = False)
            locallist.append([d1,rows])
        locallist = sorted(locallist, key=lambda x: x[1][0]['bonf_corr_pval'], reverse = False)
        CUIdict[drug] = locallist
    json.dump(CUIdict, open(base_path + "data/repurposedb_cui.json", 'wb'))
else:
    CUIdict = json.load(open(base_path+"data/repurposedb_cui.json"))

#Load ADMET data from webscraped csv
evidence = csvToDictionary(base_path+ 'data/evidence.csv', 'RxID')
evidenceTypes = {'True' :{'True' :{'True' :[3,0,'Post-hoc validation using evidence types: EHR, SGA, and P-CT'],
                                   'False':[2,1,'Post-hoc validation using evidence types: EHR and SGA']},
                          'False':{'True' :[2,1,'Post-hoc validation using evidence types: EHR and P-CT'],
                                   'False':[1,2,'Post-hoc validation using evidence types: EHR']}
                         },
                 'False':{'True' :{'True' :[2,1,'Post-hoc validation using evidence types: SGA and P-CT'],
                                   'False':[1,2,'Post-hoc validation using evidence types: SGA']},
                          'False':{'True' :[1,2,'Post-hoc validation using evidence types: P-CT'],
                                   'False':[0,3,'No Post-hoc validation']}
                         }
                 }

pvs = csvToDictionary(base_path+'data/Repurposedb_semantic_sim_final.csv', 'RxID')

print("Server is Up!")

import lib.flaskIndex